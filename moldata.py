# -*- coding: utf-8 -*-
"""molecular database


special dependencies: Bottle, Dataset, operator
(c) Sylvester Zhang 2018
license: AGPLv3

   
   Copyright 2018 randon <randon@randonmac>
   
   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU Affero General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Affero General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
   MA 02110-1301, USA.

"""
from bottle import get, post, request, run, redirect, template
import dataset
from difflib import SequenceMatcher
import operator
import os

molDB = dataset.connect('sqlite:///molecule.db')


@get('/search')
def handleSearch():
    #with open('cards_against_start.html') as f:
    #    u = f.read()
    #udict = {'timefetched':time.strftime("%c"),}
    #return template(u, **udict)
    return '''
        <form action="/search" method="post">
            Query: <input name="query" type="text" />
            <input value="Search" type="submit" />
        </form>
    '''
@post('/search')
def handleSearchPost():
    query = request.forms.get('query')
    searchRes = search(query)
    molecules = molDB['molecules']
   
    if searchRes['status']:
        redirect('/mol/'+search(query)['tradeName'])
    else:
        #searchRes['possibleMatches'] == {'1223-33-2':0.1, ... }
        #
        possibleMatches = searchRes['possibleMatches']
        result = []
        for substance in possibleMatches:
            #[('casNumber',likelihood)]
            common_names = molecules.find_one(tradeName=substance[0]).get('tradeName')
            result.append((substance[0],str(common_names)))
        with open('search_results.html') as f:
            u = f.read()
        return template(u,**{'results':result, 'numberReturned':str(len(result))})


@get('/mol/<trade_name>')
def handleMol(trade_name):
    molecules = molDB['molecules']
    substance = molecules.find_one(tradeName=trade_name)
    if type(substance) != type(None):
        molecules = molDB['molecules']
        substance = molecules.find_one(tradeName=trade_name)
        with open('cas_result.html') as f:
            u = f.read()
        return template(u,**{'tradeName':trade_name,'substance':substance})
        #do some stuff to prettify it
        return str(substance)
    else:
        redirect('/search')
        
@get('/edit/<trade_name>')
def getEdit(trade_name):
    molecules = molDB['molecules']
    #print(trade_name)
    with open('edit.html') as f:
        u = f.read()
    moleculeData = molecules.find_one(tradeName=trade_name)
    if type(moleculeData)==type(None):
        molecules.insert(dict(tradeName=trade_name))
        moleculeData = molecules.find_one(tradeName=trade_name)
    moleculeData = dict(moleculeData)
    return template(u,**moleculeData)
    #return str(moleculeData)
        
@post('/edit/<trade_name>')
def postEdit(trade_name):
    molecules = molDB['molecules']
    data = request.forms
    data = dict(data)
    NMR = request.files.get('NMR')
    print(type(NMR))
    if type(NMR) !=type(None): #check if NMR value is changed. If it is left blank, then 
                               #delete the entry NMR from the update data, so we dont overwrite anything. 
        print('nmr is not NoneType')
        name, ext = os.path.splitext(NMR.filename)
        if ext not in ('.png','.jpg','.jpeg'):
            return 'File extension not allowed.'
        NMR.save(trade_name+'.tmp')
        with open(trade_name+'.tmp','rb') as f: #save the image to disk, then delete it
            NMR=f.read()
        data.update({'NMR':NMR}) #
    
        os.remove(trade_name+'.tmp')
    else:
        data.pop('NMR')
    #    print(str(data))
    molecules.update(data,['tradeName'])
    #print(str(data))
    redirect('/mol/'+trade_name)
        
        
        
        
def similar(a, b):
    return SequenceMatcher(None, a, b).ratio()

def search(query): #takes a query as a CAS number, trade name, IUPAC name, common names
    molecules = molDB['molecules']
    possibleMatches={}
    print(query)
    for substance in molecules:
        if query == substance['casNumber']:# query is first assumed to be a cas number
            return {'status':True,'tradeName':substance['tradeName']}
        #If we get here, the query is not the CAS number for current substance. 
        #We try to see if it is a trade name. 
        if query == substance['tradeName']:
            return {'status':True,'tradeName':substance['tradeName']}
        #if we get here, the query is not a trade name or cas number of current item.
        #check if it is a common name
        #if query in json.loads(substance['commonNames']):
        #    return {'status':True,'casNumber':substance['casNumber']}
        #check if it is an IUPAC name
        if query == substance['structuralName']:
            return {'status':True,'tradeName':substance['tradeName']}
        #return {'status':True,'casNum':casNumber}
    #break out of loop if not found. Now we suggest the most similar. We omit Cas numbers
    #as those are not assigned linearly. Search by trade name and common names
    for substance in molecules:
        #use string matching of query to known names
        names = substance['tradeName']# +";"+substance['structuralName']
        names = names.split(';')
        keyIdentity={substance['tradeName']:0}
        for name in names:
            keyIdentity.update({substance['tradeName']:keyIdentity.get(substance['tradeName'])+similar(query,name),})
        possibleMatches.update(keyIdentity)
    sorted_possibleMatches = sorted(possibleMatches.items(), key=operator.itemgetter(1),reverse=True)
    
    
    
    return {'status':False,'possibleMatches':sorted_possibleMatches}












run(host='localhost', port=8080, debug=True)