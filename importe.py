import dataset
import csv
molDB = dataset.connect("sqlite:///molecule.db")

molecules = molDB['molecules']

"""
0 - manufacturer
1 - tradeName
2 - activeFraction
3 - form
4 - pH
5 - biodegradable
6 - toxicity
7 - comments
8 - [blank]
9 - HLB
10 - structuralName
11 - casNumber
12 - structureClass
13 - health
14 - flammability
15 - reactivity
16 - ppe
17 - NFPAFlammClass
18 - shelf
19 - borrowedBy
"""
with open('molecules.csv','r') as csvfile:
    readCSV = csv.reader(csvfile)
    for row in readCSV:
        #print(row)
        molecules.insert(dict(manufacturer=row[0],tradeName=row[1],activeFraction=row[2],form=row[3],
pH=row[4],biodegradable=row[5],toxicity=row[6],comments=row[7],HLB=row[9],structuralName=row[10],
casNumber=row[11],structureClass=row[12],health=row[13],flammability=row[14],reactivity=row[15],
ppe=row[16],NFPAFlammClass=row[17],shelf=row[18],borrowedBy=row[19]))
